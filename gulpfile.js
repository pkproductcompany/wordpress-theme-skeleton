var gulp = require('gulp');
var minifyCss = require('gulp-minify-css');
var gulpBabel = require('gulp-babel');
var sass = require('gulp-sass');
var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
var prettier = require('gulp-prettier');

/*
Setting compiler for SASS
 */
sass.compiler = require('node-sass');

/*
Converting SASS files to CSS,
Apply prefix to all required CSS properties
Minify CSS files and
Output the minified CSS file.
 */
gulp.task('sass', () => {
    return gulp.src('*.scss')
        .pipe(sass({}).on('error', sass.logError))
        .pipe(postcss([autoprefixer()]))
        .pipe(minifyCss())
        .pipe(gulp.dest('./'));
});


/*
Converting JS files to compatible JS files for (IE10 and IE11),
Apply prettier code format,
Output the JS file to dist directory.
 */
gulp.task('js', () => {
    return gulp.src('js/*.js')
        .pipe(gulpBabel({
            presets: ['@babel/env']
        }))
        .pipe(prettier({singleQuote: true, tabWidth: 4}))
        .pipe(gulp.dest('dist/'));
});

/*
Set up watcher for SCSS and JS files, in case of change to them, it runs tasks.
 */
gulp.task('watch', () => {
    gulp.watch('*.scss', gulp.series('sass'));
    gulp.watch('js/*.js', gulp.series('js'));
});

/*
Starting point of gulp
 */
gulp.task('default', gulp.series('sass', 'js', 'watch'));