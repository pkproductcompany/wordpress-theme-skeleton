# WordPress Theme Skeleton

Use this repo for all WordPress theme development project. 

Before you clone the project, go to the appropriate directory for WordPress theme then clone the WordPress Theme Skeleton.


Once you clone the project, change the directory to the project and install the package.json by running the following command:

```
npm install 
```

OR

```
yarn install
```

Upon packages installation, You have to run gulp to enable watcher to watch SCSS and JS files so gulp can generate 
CSS and JS which are compatible to IE 10 and IE 11 browsers. 

```
./node_modules/.bin/gulp
```